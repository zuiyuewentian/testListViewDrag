﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace testListViewDrag
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }


        private void button1_Click(object sender, EventArgs e)
        {
            string url = "tempPic\\";
            DirectoryInfo dir = new DirectoryInfo(url);
            FileSystemInfo[] fileinfos = dir.GetFiles();  //返回目录中所有文件和子目录

            foreach (var item in fileinfos)
            {
                var img = Image.FromFile(url + item);
                imageList1.Images.Add(img);
            }
            this.listView1.View = View.LargeIcon;
            this.listView1.LargeImageList = imageList1;
            for (int i = 0; i < imageList1.Images.Count; i++)
            {
                ListViewItem item = new ListViewItem();
                item.Text = i.ToString();
                item.ImageIndex = i;
                listView1.Items.Add(item);
            }
        }

        ListViewItem moveItem = null;

        private void listView1_ItemDrag(object sender, ItemDragEventArgs e)
        {
            ListViewItem item = (ListViewItem)e.Item as ListViewItem;

            moveItem = item;
            Cursor.Current = Cursors.SizeAll;
        }

        private void listView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (moveItem == null)
                return;

            // 获得鼠标坐标 
            Point point = listView1.PointToClient(new Point(e.X, e.Y));
            // 返回离鼠标最近的项目的索引 
            int index = listView1.InsertionMark.NearestIndex(point);
            //移动项的索引
            int mIndex = moveItem.Index;
            if (mIndex == index)
                return;

            var img = imageList1.Images[mIndex];
            var changeItem = listView1.Items[mIndex];
            string text = changeItem.Text;
            if (mIndex > index)
            {
                //从后往前移动
                for (int i = mIndex; i >= index; i--)
                {
                    if (i == 0)
                        continue;
                    imageList1.Images[i] = imageList1.Images[i - 1];
                    listView1.Items[i].Text = listView1.Items[i - 1].Text;
                }
                imageList1.Images[index] = img;
                listView1.Items[index].Text = text;

            }
            else
            {
                //从前往后
                for (int i = mIndex; i < index; i++)
                {
                    imageList1.Images[i] = imageList1.Images[i + 1];
                    listView1.Items[i].Text = listView1.Items[i + 1].Text;
                }
                imageList1.Images[index] = img;
                listView1.Items[index].Text = text;
            }

            this.listView1.LargeImageList = imageList1;

            this.listView1.Refresh();
            moveItem = null;
            Cursor.Current = Cursors.Default;
        }

        private void listView1_MouseMove(object sender, MouseEventArgs e)
        {
            if (moveItem == null)
                return;

            Cursor.Current = Cursors.SizeAll;
        }
    }
}
